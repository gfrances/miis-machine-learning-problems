%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Basic Decision Theory}
\label{sec:basic-decision-theory}
%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Problem 1}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
We have that $p =  \Prob{Y = 1}$. The independence assumption means that $$\eta(x) = \Prob{Y = 1 | X = x} = \Prob{Y = 1} = p$$

Now, we could apply the alternative Bayes risk definition that we saw in class
(i.e., $R^* = \E [\min(\eta(x), 1-\eta(x)) ]$ ), which results in 
$R^* = \E [\min(p, 1-p) ]$, 
and since $p$ does not depend on $X$, this directly implies
$R^* = \min(p, 1-p) $

Alternatively, we can go all the way from applying the basic definition of Bayes Risk.
From our re-writing of $\eta(x)$, we can say that the output of the Bayes classifier is totally independent of the value of $x$:

$$
g^*(x) = 
\begin{cases}
  1 & \text{if $p > 1/2$} \\
  0 & \text{otherwise}
\end{cases}
$$

On the other hand, the Bayes risk is
$$
R^* = \Prob{g^*(x) \neq Y} =
\Prob{g^*(x) = 0 \land Y = 1} + \Prob{g^*(x) = 1 \land Y = 0}
$$

Now, the value of $p$ is a given constant. Suppose that $p > 1/2$ (i.e. $p > 1-p$, or $1-p = \min(p, 1-p)$).
Then we have that the Bayes classifier will always classify as $1$,
which means that  $R^* = \Prob{Y = 0} = 1-p = \min(p, 1-p)$

Conversely, suppose that $p \leq 1/2$ (i.e. $p \leq 1-p$, or $p = \min(p, 1-p)$).
Then the classifier will always say $0$, thus 
$R^* = \Prob{Y = 1} = p = \min(p, 1-p)$

In both cases, we have that $R^* = \min(p, 1-p)$
\qed


For the second part of the exercise, consider any joint distribution such that $\Prob{Y = 1 | X = x} > 1/2$ for all possible $x$.
This does not necessarily imply independence, as the actual posterior probabilities can take different values depending on $x$ (say,
$\Prob{Y = 1 | X = x} = a$ when $x < 0$ and $\Prob{Y = 1 | X = x} = b$ otherwise, with both $a, b > 1 / 2$).
With this distribution, 
$p = \Prob{Y = 1} > 1/2$, since it is so conditioned to any possible value of $x$.
At the same time, $g^*(x)=1$, thus the Bayes risk is $R^* = \Prob{Y = 0} = 1-p = \min (p, 1-p)$, which is what we wanted.



\subsection{Problem 2}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
Let $g^*_{X_{1}}$ be one of the classifiers based on the observation of $X_1$ that achieve the minimum Bayes risk $R(g^*_{X_{1}}) = R^*_{X_{1}}$.
Now, let $g'$ be a classifier based on the observation of $X' = (X_1, X_2)$ that simply ignores $X_2$ and proceeds as the previous classifier:
$g'(x_1, x_2) = g^*_{X_{1}}(x_1)$. Clearly, $R(g') = R(g^*_{X_{1}})$, and therefore the minimum risk of any classifier
based on $X'$ will be at most this value, $R^*_{X'} \leq R(g') = R(g^*_{X_{1}})$

Now, if $X_2$ is independent of $(X_1, Y)$, the observation of $X_2$ cannot give any insight to the classifier.
Formally, we have that because of the independence, the Bayes classifiers for both distributions are ultimately based on the same posterior probability:
$$
\eta'(x_1, x_2) = \Prob{Y = 1 | X_1 = x_1, X_2 = x_2} = \Prob{Y = 1 | X_1 = x_1} = \eta(x_1)
$$

Therefore, the Bayes classifier that classifies optimally with risk $R^*_{X'}$ based on $X' = (X_1, X_2)$ will be
$$
g^*_{X'}(x_1, x_2) = 
\begin{cases}
  1 & \text{if $\eta'(x_1, x_2) > 1/2$} \\
  0 & \text{otherwise}
\end{cases}
= 
\begin{cases}
  1 & \text{if $\eta(x_1) > 1/2$} \\
  0 & \text{otherwise}
\end{cases}
$$

which is precisely the optimal Bayes classifier with risk $R^*_{X_{1}}$.

\qed


\subsection{\textcolor{red}{Problem 3}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
Ask Alberto.

\subsection{Problem 4}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
We need to prove that $\Abs{R(g_1) - R(g_2)} \leq \Prob{g_1(X) \neq g_2(X)}$.
The basic intuition is that the left-hand side of the expression can only increase in those $x$ where
both classifiers give different predictions, and whenever that happens, the righ-hand side will increase as well.

% Thus, we will have strict equality whenever that every time that the classifiers differ,
% it is always the same classifier the one that errs the classification.


Let $\rho(x) = 1 - 2\eta(x)$; we have that for any $x$, $-1 \leq \rho(x) \leq 1$.
Let's prove the assertion for a discrete domain. From the definition of risk, we have that

$$
R(g) = \sum_{x \in \mathcal{X}} \Prob{X=x} \cdot \Brack{\eta(x) \cdot \mathbf{1}_{g(x)=0} (x) + (1-\eta(x)) \cdot \mathbf{1}_{g(x)=1} (x) }
$$

Now, let's assume without loss of generality that $R(g_1) > R(g_2)$, so that we can get rid of the absolute value operator.
Subtracting the risk of the two classifiers:

$$
R(g_1) - R(g_2) = \sum_{x \in \mathcal{X}} \Prob{X=x} \cdot \Brack{
\rho(x) \cdot (\mathbf{1}_{g_1(x)=1} (x) - \mathbf{1}_{g_1(x)=1} (x)) \cdot (\pm 1)
}
$$

We need only realize that whenever $g_1(x) = g_2(x)$, the previous expression will be zero, so the sum can be decomposed into:

\begin{align*}
R(g_1) - R(g_2) &=
\Parent{\sum_{x \in \mathcal{X} : g_1(x) = 1 \neq g_2(x)} \Prob{X=x} \cdot \rho(x)} \\
&+ \Parent{\sum_{x \in \mathcal{X} : g_1(x) = 0 \neq g_2(x)} \Prob{X=x} \cdot -\rho(x)}
\end{align*}

Since $-1 \leq \rho(x), -\rho(x) \leq 1$, we have that 

\begin{align*}
R(g_1) - R(g_2) &\leq
\Parent{\sum_{x \in \mathcal{X} : g_1(x) = 1 \neq g_2(x)} \Prob{X=x}} \\
&+ \Parent{\sum_{x \in \mathcal{X} : g_1(x) = 0 \neq g_2(x)} \Prob{X=x}} \\
&=
\sum_{x \in \mathcal{X} : g_1(x) \neq g_2(x)} \Prob{X=x} \\
&= \Prob{g_1(X) \neq g_2(X)}
\end{align*}

\qed

From the proof we can also see that we will have strict equality if for any $x$, where the classifiers differ,
we have $\eta(x) \in \{0, 1\}$, and it is always the same classifier that errs the prediction ($g_1$ in our equations).


