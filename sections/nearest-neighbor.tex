%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Nearest Neighbor Classification}
\label{sec:nearest-neighbor}
%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Problem 5}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
We want to prove that
$$
\lim_{n \to \infty} || X_{k} - X || = 0
$$
in probability, i.e. that for any $\delta > 0$
$$
\lim_{n \to \infty} \Prob{|X_{k} - X| \geq \delta} =  0
$$
or, equivalently
$$
\lim_{n \to \infty} \Prob{ |X_{k} - X| < \delta} =  1
$$
Now, fix a $\delta$ and let us partition the $n$ random vectors into $k_n$ groups of equal size $n / k_n$.
If we denote by $p$ the \emph{probability that in every such group there is at least one random vector $X'$ such that $|X' - X| < \delta$},
we have that 

$$
\Prob{ |X_{k} - X| < \delta} \geq p
$$

Let's now examine $p$, which is the probability of $k_n$ independent events:
\begin{align*}
p = \prod_{1 \leq j \leq k_n} (1 - p_j)
\end{align*}

where $p_j$ is the probability that for the $j-th$ group ($ 1 \leq j \leq k_n$) all elements $X^{(j)}_i$ ($1 \leq i \leq n / k_n$)
are such that $|X^{(j)}_i - X| < \delta$. This $p_j$ is
$$
p_j = \prod_{1 \leq i \leq n / k_n} \Prob{ |X^{(j)}_i - X| > \delta}
$$

Given that by assumption
$\lim_{n \to \infty} k_n/n = 0$
, i.e. $\lim_{n \to \infty} n/k_n = \infty$, the limit of this product $p_j$ as $n \to \infty$
will be a product with an infinite number of multipliers smaller than $1$, thus tending to $0$.
Therefore, $p \to 1$, which lets us prove that 
$$
\lim_{n \to \infty} \Prob{ |X_{k} - X| < \delta} \geq \lim_{n \to \infty} p = 1
$$
\qed


\subsection{Problem 6}
%%%%%%%%%%%%%%%%%%%%%%%%%%%

Applying the definition of risk as we saw in class, we have that, asymptotically,
\begin{align*}
R_{k-NN} &= \Expect{(1-\eta(x)) \cdot \Prob{Bin(k, \eta(x)) > k/2} } \\
&+ \Expect{ \eta(x) \cdot \Prob{Bin(k, \eta(x)) \leq k/2} } \\
&= \Expect{ \eta(x) + (1-2\eta(x)) \cdot \Prob{Bin(k, \eta(x)) > k/2} }
\end{align*}

Now, let $p = \eta(x)$. If $p \leq 1-p$, i.e. $p = \min(p,1-p)$, and $\abs{2p-1} = 1-2p$, the expression we want to prove follows
directly from replacing the values in the last equation.
Now, suppose that $p > 1-p$, i.e. $1-p = \min(p,1-p)$, and $\abs{2p-1} = 2p-1$.
We can reformulate our last equation by taking into account the property that (assuming $k$ is odd)
$$
\Prob{Bin(k,p) > k/2} = \Prob{Bin(k,1-p) < k/2} = 1 - \Prob{Bin(k,1-p) > k/2}
$$

Thus, substituting and rearranging terms:
\begin{align*}
R_{k-NN} &= \Expect{ \eta(x) + (1-2\eta(x)) \cdot \Prob{Bin(k, \eta(x)) > k/2} } \\
&= (1-\eta(x)) + (2\eta(x) - 1) \cdot \Prob{Bin(k,1-p) > k/2}
\end{align*}

which, again, is what we wanted to prove.
\qed

\subsection{Problem 7}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
We know that $R^* = \Expect{\min(\eta(x), 1-\eta(x))}$. Subtracting from the expression of the previous exercise,
we have that
$$
R_{k-NN} - R^* =  \Expect{ \abs{2\eta(x)-1} \cdot \Prob{Bin(k, \min(\eta(x), 1-\eta(x)) )>k/2} }
$$

Let $p_x = \min(\eta(x), 1-\eta(x)) \leq 1/2$. It can be seen that (for any $0 \leq p \leq 1$) $\abs{2p-1} = 1-2 \cdot \min(p, 1-p)$.
Thus, the previous expression can be rewritten as 

\begin{align*}
R_{k-NN} - R^* &=  \Expect{ (1-2p_x) \cdot \Prob{Bin(k, p_x ) > k/2} } \\
&\leq \sup_{p \in [0, 1/2]} (1-2p) \cdot \Prob{Bin(k, p) > k/2}
\end{align*}

(since the expectation of a value will be always less or equal than the supremum).

Now, dividing both sides of $\Prob{Bin(k, p) > k/2}$ by $k$ and subtracting $p$, we can apply Hoeffding's Inequality:
\begin{align*}
\Prob{Bin(k, p) > k/2} 
&= \Prob{\frac{Bin(k, p)}{k} - p > \frac{1-2p}{2}}  \\
% &\leq \exp(-2k(\frac{1-2p}{2})^2)
&\leq e^{-2k(\frac{1-2p}{2})^2}
\end{align*}

This value tends to zero as $k \to \infty$, therefore we have that 
\begin{align*}
\lim_{k \to \infty} R_{k-NN} - R^*
&\leq \lim_{k \to \infty} \sup_{p \in [0, 1/2]} (1-2p) \cdot e^{-2k(\frac{1-2p}{2})^2}
= 0
\end{align*}

\qed

\subsection{Problem 8}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
We know that $\Prob{X|Y=0} \sim \mathcal{U}(0,1)$, and $\Prob{X|Y=1} \sim \mathcal{U}(2,3)$,
and that $\Prob{Y=0} = \Prob{Y=1} = \frac{1}{2}$. Let $p$ denote this probability $\frac{1}{2}$.
The basic fact (think on one dimension) is that all the points labeled $0$ lie on the $[0,1]$ interval, while all the points
labeled $1$ lie on the $[2,3]$ interval.

For the $1-NN$ classifier, this means that the classifier will only make a mistake on a given point
if \emph{all} the training points lie on the other interval, which has a very small probability.
For a $k-NN$ strategy, the classifier will make a mistake on a given point as long as only
less than $\frac{k}{2}$ lie on the same interval, and this event has a larger probability.

Formally, the risk of the $1-NN$ classifier is
\begin{align*}
R_{1-NN} &= \Prob{Y=0} \cdot \Prob{\textrm{the n training points have label Y=1}} \\
&+ \Prob{Y=1} \cdot \Prob{\textrm{the n training points have label Y=0}} \\
&= \Prob{Y=0} \cdot \Prob{Y=1}^{n} + \Prob{Y=1} \cdot \Prob{Y=0}^{n} \\
&= 2p^{n+1} = p^{n}
\end{align*}

(note that in the last step we make use of the fact that $p = \frac{1}{2}$).

While the risk of the $k-NN$ classifier is (assuming $k$ is odd)
\begin{align*}
R_{1-KK} &= \Prob{Y=0} \cdot \Prob{Bin(n, \Prob{Y=1}) \geq \ceil{k/2}} \\
&+ \Prob{Y=1} \cdot \Prob{Bin(n, \Prob{Y=0}) \geq \ceil{k/2}} \\
&= \Prob{Bin(n, p) \geq \ceil{k/2}} = \sum_{i=\ceil{k/2}}^{n} \Prob{Bin(n, p) = i} \\
&= p^n + \sum_{i=k/2}^{n-1} \Prob{Bin(n, p) = i}
\end{align*}

Now, this probability is strictly larger than $p^n = R_{1-NN}$, as the second term of the sum is always some strictly positive probability (as long as $n > 2$!).
This is exactly what we wanted to prove.
\qed







% \subsection{Problem 9}
\subsection{\textcolor{red}{Problem 9}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
We want to prove that the rule is consistent, i.e. that $\lim_{n \to \infty} R(g_n) = R^{*}$.
The intuition is that we can probably choose a $h_n$ such that as $n \to \infty$
\begin{enumerate}
 \item  The size $h^n$ of the cubes shrinks, and
 \item  The number $k$ of points that \emph{fall} into any given cube (which depends on the density of $\Prob{X}$) grows.
\end{enumerate}

The first condition allows us to assume that for any given point $x$, \emph{all} points $x_i$ in its same cube
share the same \emph{a posteriori} probability $\Prob{Y = 1 | X = x_i} = \eta(x)$.

The second condition allows us to assume that, since we will have more and more points in the cube where $x$ falls,
we will have an increasingly good approximation of the real $\eta(x)$.




% \subsection{Problem 10}
\subsection{\textcolor{red}{Problem 10}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  \lim_{x \to +\infty}, \inf_{x > s} and \sup_K